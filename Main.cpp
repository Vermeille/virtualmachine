#include "CPU.h"
#include "Instructions.h"
#include "Register.h"
#include <string>
#include <fstream>
#include <iostream>
#include <iterator>

int main(int argc, char** argv)
{
    CPU cpu;
    std::string printer[256];
	cpu.RegisterOpcode<0x1, ADD<Register<A>, Register<A>>>();
#define X(Opcode, Template)                 \
    cpu.RegisterOpcode< Opcode, Template >(); \
    printer[Opcode] = #Template;
# include "instructions.def"
#undef X

	auto& ram = cpu.Ram();
    //std::ifstream f(argv[1]);
    //std::copy(std::istream_iterator<unsigned char>(f), std::istream_iterator<unsigned char>(), std::begin(ram));

    while (true)
    {
        cpu.Process();
        std::cout << std::hex << cpu.Pc() << "\t" << printer[cpu.Pc()] << std::endl;
    }
    return 0;
}