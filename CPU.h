#pragma once

#include <cstdint>
#include <functional>
#include <vector>

typedef uint8_t byte;
typedef uint32_t dword;

class CPU
{
    public:
        typedef std::vector<uint8_t> ram_t;
        CPU();
    
        ~CPU();
        
        ram_t& Ram(); 
        
        uint32_t Sp() const;
        void Sp(uint32_t sp);
        
        uint32_t Pc() const;
        void Pc(uint32_t pc);

        template <int Opcode, class Instr>
        void RegisterOpcode();
        
        void Process();

        ram_t ram_;
    private:        
        std::function<void(CPU&)> instr_[256];
        uint32_t pc_;
        uint32_t sp_;
};

template <int Op, class Instr>
void CPU::RegisterOpcode()
{
    instr_[Op] = &Instr::Do;
}

