#include "CPU.h"

CPU::CPU()
	: ram_(512 * 1024 * 1024), // 512 Mo
	pc_(0), // program starts at 0x0
	sp_(2048) // memory at address 0x20000
{
}

CPU::~CPU() { }

CPU::ram_t& CPU::Ram() { return ram_; }

uint32_t CPU::Sp() const { return sp_; }
void CPU::Sp(uint32_t sp) { sp_ = sp; }

uint32_t CPU::Pc() const { return pc_; }
void CPU::Pc(uint32_t pc) { pc_ = pc; }

void CPU::Process()
{
    instr_[pc_](*this);
    ++pc_;
}