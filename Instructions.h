#pragma once

#define ARITH(Name, Op)      \
template <class A, class B>  \
struct Name                   \
{                            \
    static inline void Do(CPU& cpu)  \
    {                                \
        A::Set(cpu, A::Get(cpu) Op B::Get(cpu)); \
    }						\
};							\

ARITH(ADD, +)
ARITH(SUB, -)
ARITH(DIV, /)
ARITH(MUL, *)

template <class Target>
struct JP
{
    static inline void Do(CPU& cpu)
    {
        cpu.Pc() = Target::Get(cpu);
    }
};

template <class Target>
struct CALL
{
    static inline void Do(CPU& cpu)
    {
        cpu.Sp(cpu.Sp() + 4);
        static_cast<uint32_t&>(cpu.ram[cpu.Sp()]) = cpu.Pc(); // WARN: endianness
        JP<Target>::Do(cpu);
    }
};

template <class Src>
struct PUSH
{
    static inline void Do(CPU& cpu)
    {
        cpu.Sp(cpu.Sp() + 4);
        cpu.Sp() = Target::Get(cpu);
    }
};


struct POP
{
    static inline void Do(CPU& cpu)
    {
        cpu.Sp(cpu.Sp() - 4);
    }
};

struct NOP
{
    static inline void Do(CPU& cpu) {}
};

