#pragma once

#include "CPU.h"

enum RegisterEnum
{
	A = 0,
	B,
	C,
	D,
	E
};


template <int R>
struct Register
{
	static inline dword Get(CPU& cpu)
	{
		return cpu.Ram()[cpu.Sp() - R * 4];
	}

	static inline void Set(CPU& cpu, byte val)
	{
		cpu.Ram()[cpu.Sp() - R * 4] = val;
	}
};

/* This class allows absolute adressing */
template <class Dst>
class ToAddr
{
    static inline dword Get(CPU& cpu)
    {
        return cpu.Ram()[Dst::Get(cpu)];
    }
    
    static inline void Set(CPU& cpu, byte val)
    {
        cpu.Ram()[Dst::Get(cpu)] = val;
    }
};

/* And this one relative Addressing */
template <class Dst>
class ToAddrRelative
{
    static inline dword Get(CPU& cpu)
    {
        return cpu.Ram()[cpu.Sp() + Dst::Get(cpu)];
    }
    
    static inline void Set(CPU& cpu, byte val)
    {
        cpu.Ram()[cpu.Sp() + Dst::Get(cpu)] = val;
    }
    
    static inline void Set(CPU& cpu, dword val)
    {
        static_cast<uint32_t&>(cpu.Ram()[cpu.Sp() + Dst::Get(cpu)]) = val;
    }
};